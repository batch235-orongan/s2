const { expect, assert } = require("chai");
const { getCircleArea, checkIfPassed, getAverage, getSum, getDifference, factorial, div_check } = require("../util.js");


describe("Test get area circle", () => {
	it("Test area of circle radius 15 is 706.86", () => {

		let area = getCircleArea(15);
		assert.equal(area, 706.86);
	})

	it("Test area of circle with radius of 9 is 254.46959999999999", () => {

		let area = getCircleArea(9);
		expect(area).to.equal(254.46959999999999);
	})
})

describe("Test check if passed", () => {

	it("Test 25 out of 30 is passed", () => {

		let ispassed = checkIfPassed(25, 30);
		assert.equal(ispassed, true);
	})

	it("Test 30 out of 50 is not passed", () => {
		let ispassed = checkIfPassed(30, 50);
		assert.equal(ispassed, false);
	})
})

describe("Test get average", () => {
	it("Test the average of 80, 82, 84 and 86 is 83", () => {

		let average = getAverage(80, 82, 84, 86);
		assert.equal(average, 83);
	})
	it("Test the average of 70, 80, 82 and 84 is 79", () => {

		let average = getAverage(70, 80, 82, 84);
		expect(average).to.equal(79);
	})
})


describe("Test get sum", () => {
	it("Test the sum of 15 and 30 is 45", () => {

		let sum = getSum(15, 30);
		assert.equal(sum, 45);
	})
	it("Test the sum of 25 and 50 is 75", () => {

		let sum = getSum(25, 50);
		expect(sum).to.equal(75);
	})
})

describe("Test get difference", () => {
	it("Test the difference of 70 and 40 is 30", () => {

		let difference = getDifference(70, 40);
		assert.equal(difference, 30);
	})
	it("Test the difference of 125 and 50 is 75", () => {

		let difference = getDifference(125, 50);
		expect(difference).to.equal(75);
	})
})

describe("Test factorial", () => {
	it("Test that 5! is 120", () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it("Test that 1! is 1", () => {

		const product = factorial(1);
		expect(product).to.equal(1);
	})

	it("Test that 0! is 1", () => {

		const product = factorial(1);
		assert.equal(product, 1);
	})

	it("Test that -1! is undefined", () => {

		const product = factorial(-1);
		assert.equal(product, undefined);
	})

	it("Test that a! is undefined", () => {

		const product = factorial("a");
		expect(product).to.equal(undefined);
	})
})

// activity

describe("Test divisible check", () => {
	it("Test that 120 is divisible by 5", () => {
		let div = div_check(120);
		expect(div).to.equal(true);
	})
	it("Test that 14 is divisible by 7", () => {
		let div = div_check(14);
		expect(div).to.equal(true);
	})
	it("Test that 105 is divisible by 5 OR 7", () => {
		let div = div_check(105);
		expect(div).to.equal(true);
	})
	it("Test that 22 is not divisible by 5 OR 7", () => {
		let div = div_check(22);
		assert.notEqual(div, true);
	})
})